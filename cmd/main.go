package main

import (
	"github.com/gin-gonic/gin"
	rw "nbastats/internal/responsewriter"
	"nbastats/internal/statscl"
	"net/http"
)

func getPlayers(c *gin.Context) {
	var players = new(rw.Players)
	statsClient, req := statscl.New("/commonallplayers", c.Request.URL.Query())

	res := statsClient.SendRequest(req)

	players.Unmarshall(res)
	rw.WriteRawJSON(c, players)

}

func getPlayer(c *gin.Context) {
	var player rw.Player
	statsClient, req := statscl.New("/commonplayerinfo", c.Request.URL.Query())

	res := statsClient.SendRequest(req)

	if res.StatusCode == http.StatusOK {
		player.Unmarshall(res)
		rw.WriteRawJSON(c, player)
		return
	}
	rw.WriteNotFound(c)
}

func getTeam(c *gin.Context) {
	var team rw.Team
	statsClient, req := statscl.New("/teaminfocommon", c.Request.URL.Query())

	res := statsClient.SendRequest(req)

	if res.StatusCode == http.StatusOK {
		team.Unmarshall(res)
		rw.WriteRawJSON(c, team)
		return
	}
	rw.WriteNotFound(c)
}

func getTeams(c *gin.Context) {
	var teams = new(rw.Teams)
	statsClient, req := statscl.New("/commonteamyears", c.Request.URL.Query())

	res := statsClient.SendRequest(req)

	teams.Unmarshall(res)
	rw.WriteRawJSON(c, teams)
}

func main() {
	router := gin.Default()

	nba := router.Group("/nba")
	{
		nba.GET("/player", getPlayer)
		nba.GET("/players", getPlayers)
		nba.GET("/team", getTeam)
		nba.GET("/teams", getTeams)
	}
	router.Run(":8080")
}
