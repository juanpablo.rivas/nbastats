FROM golang:alpine AS builder

#Install git to fetch dependencies
RUN apk update && apk add --no-cache git

WORKDIR $GOPATH/nbastats
COPY . .

#Fetch dependencies
RUN go get -d -v
#Build binary
RUN go build ./cmd/main.go -o /go/bin/nbastats

FROM scratch

COPY --from=builder /go/bin/nbastats /go/bin/nbastats

ENTRYPOINT ["/go/bin/nbastats"]