package responsewriter

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
)

type Players struct {
	All []Player `json:"all"`
}

type Teams struct {
	All []Team `json:"all"`
}

type Player struct {
	ID               float64 `json:"player_id"`
	FirstName        string  `json:"first_name,omitempty"`
	LastName         string  `json:"last_name,omitempty"`
	DisplayFirstLast string  `json:"display_first_last,omitempty"`
	Birthdate        string  `json:"birthdate,omitempty"`
	Height           string  `json:"height,omitempty"`
	Weight           string  `json:"weight,omitempty"`
	Jersey           string  `json:"jersey,omitempty"`
	Position         string  `json:"position,omitempty"`
	Team             Team    `json:"team,omitempty"`
}

type Team struct {
	ID           float64 `json:"team_id"`
	City         string  `json:"city,omitempty"`
	Name         string  `json:"name,omitempty"`
	Abbreviation string  `json:"abbreviation,omitempty"`
	Conference   string  `json:"conference,omitempty"`
	Division     string  `json:"division,omitempty"`
}

type playersInfo struct {
	Resource   string `json:"resource"`
	Parameters struct {
		LeagueID            string `json:"LeagueID"`
		Season              string `json:"Season"`
		IsOnlyCurrentSeason int    `json:"IsOnlyCurrentSeason"`
	} `json:"parameters"`
	ResultSets []struct {
		Name    string          `json:"name"`
		Headers []string        `json:"headers"`
		RowSet  [][]interface{} `json:"rowSet"`
	} `json:"resultSets"`
}

type playerInfo struct {
	Resource   string `json:"resource"`
	Parameters []struct {
		PlayerID int         `json:"PlayerID,omitempty"`
		LeagueID interface{} `json:"LeagueID,omitempty"`
	} `json:"parameters"`
	ResultSets []struct {
		Name    string          `json:"name"`
		Headers []string        `json:"headers"`
		RowSet  [][]interface{} `json:"rowSet"`
	} `json:"resultSets"`
}

type teamInfo struct {
	Resource   string `json:"resource"`
	Parameters struct {
		LeagueID   string `json:"LeagueID"`
		Season     string `json:"Season"`
		SeasonType string `json:"SeasonType"`
		TeamID     int    `json:"TeamID"`
	} `json:"parameters"`
	ResultSets []struct {
		Name    string          `json:"name"`
		Headers []string        `json:"headers"`
		RowSet  [][]interface{} `json:"rowSet"`
	} `json:"resultSets"`
}

type teamsInfo struct {
	Resource   string `json:"resource"`
	Parameters struct {
		LeagueID string `json:"LeagueID"`
	} `json:"parameters"`
	ResultSets []struct {
		Name    string          `json:"name"`
		Headers []string        `json:"headers"`
		RowSet  [][]interface{} `json:"rowSet"`
	} `json:"resultSets"`
}

func (p *Player) Unmarshall(res *http.Response) {
	var info = new(playerInfo)

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()

	if err := json.Unmarshal(body, &info); err != nil {
		fmt.Println(string(body))
		fmt.Println(err)
	}

	playerValues := info.ResultSets[0].RowSet[0]
	p.ID = playerValues[0].(float64)
	p.FirstName = playerValues[1].(string)
	p.LastName = playerValues[2].(string)
	p.Birthdate = playerValues[7].(string)
	p.Height = playerValues[11].(string)
	p.Weight = playerValues[12].(string)
	p.Jersey = playerValues[14].(string)
	p.Position = playerValues[15].(string)
	p.Team = Team{ID: playerValues[18].(float64)}

}

func (p *Players) Unmarshall(res *http.Response) {
	var info = new(playersInfo)

	body, err := ioutil.ReadAll(res.Body)
	if err != nil || body == nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()

	if err := json.Unmarshal(body, &info); err != nil {
		fmt.Println(string(body))
		fmt.Println(err)
	}

	playerValues := info.ResultSets[0].RowSet

	for _, element := range playerValues {

		p.All = append(p.All, Player{
			ID:               element[0].(float64),
			DisplayFirstLast: element[2].(string),
			Team:             Team{ID: element[8].(float64)},
		})
	}
}

func (t *Team) Unmarshall(res *http.Response) {
	var info = new(teamInfo)

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()

	if err := json.Unmarshal(body, &info); err != nil {
		fmt.Println(string(body))
		fmt.Println(err)
	}

	teamValues := info.ResultSets[0].RowSet[0]
	t.ID = teamValues[0].(float64)
	t.City = teamValues[2].(string)
	t.Name = teamValues[3].(string)
	t.Abbreviation = teamValues[4].(string)
	t.Conference = teamValues[5].(string)
	t.Division = teamValues[6].(string)
}

func (t *Teams) Unmarshall(res *http.Response) {
	var info = new(teamsInfo)

	body, err := ioutil.ReadAll(res.Body)
	if err != nil || body == nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()

	if err := json.Unmarshal(body, &info); err != nil {
		fmt.Println(string(body))
		fmt.Println(err)
	}
	teamValues := info.ResultSets[0].RowSet

	for _, element := range teamValues {
		if element[3].(string) != "2022" {
			continue
		}
		t.All = append(t.All, Team{
			ID:           element[1].(float64),
			Abbreviation: element[4].(string),
		})
	}
}

func WriteRawJSON(c *gin.Context, apiObj any) {
	c.JSON(http.StatusOK, apiObj)
}

func WriteNotFound(c *gin.Context) {
	res := make(map[string]string)
	res["message"] = "Resource Not Found"
	c.JSON(http.StatusNotFound, res)
}

func WriteInternalError(c *gin.Context) {
	res := make(map[string]string)
	res["message"] = "Internal Server Error"
	c.JSON(http.StatusInternalServerError, res)
}
