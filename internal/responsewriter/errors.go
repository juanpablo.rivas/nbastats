package responsewriter

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
)

func InternalError(res *http.Response, c *gin.Context) {
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()
	c.JSON(http.StatusInternalServerError, string(body))
}
