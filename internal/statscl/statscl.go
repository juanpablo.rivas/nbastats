package statscl

import (
	"fmt"
	"net/http"
	"net/url"
)

const baseURL string = "https://stats.nba.com/stats"

var mappings = map[string]string{"PlayerId": "id", "TeamId": "id"}

var endpoints = map[string]endpoint{
	"/commonplayerinfo": commonplayerinfo,
	"/commonallplayers": commonallplayers,
	"/teaminfocommon":   teaminfocommon,
	"/commonteamyears":  commonteamyears,
}

var commonplayerinfo = endpoint{params: map[string]string{"PlayerId": ""}}
var commonallplayers = endpoint{params: map[string]string{"LeagueID": "00", "Season": "2021-22", "IsOnlyCurrentSeason": "1"}}
var teaminfocommon = endpoint{params: map[string]string{"LeagueID": "00", "Season": "2021-22", "SeasonType": "Regular Season", "TeamId": ""}}
var commonteamyears = endpoint{params: map[string]string{"LeagueID": "00"}}

type statscl struct {
	http.Client
	EP endpoint
}

type endpoint struct {
	params map[string]string
}

func New(ep string, values url.Values) (*statscl, *http.Request) {
	var stats = statscl{EP: endpoints[ep]}

	req, _ := http.NewRequest("GET", baseURL+ep, nil)

	req.Header.Add("Accept", "application/json")
	req.Header.Add("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36")
	req.Header.Add("Referer", "https://stats.nba.com")

	stats.addQueryValues(req, values)
	return &stats, req
}

func (st *statscl) addQueryValues(req *http.Request, val url.Values) {
	qValues := req.URL.Query()

	for key, value := range st.EP.params {
		if value != "" {
			qValues.Set(key, value)
		} else {
			qValues.Set(key, val.Get(mappings[key]))
		}

	}
	req.URL.RawQuery = qValues.Encode()
}

func (st *statscl) SendRequest(req *http.Request) *http.Response {
	res, err := st.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	return res
}
